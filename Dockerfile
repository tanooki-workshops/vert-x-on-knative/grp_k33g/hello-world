# 1st Docker build stage: build the project with Maven
FROM maven:3.6.3-openjdk-11 as builder
COPY . .
RUN mvn clean package

FROM adoptopenjdk:11-jre-hotspot
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

COPY --from=builder target/*-fat.jar /app.jar
# run the application
CMD [ "java", "-jar", "/app.jar" ]

